## Routes
### `/user/get_history_card` (admin only)
#### Params
   - username = '<something>@awty.org'
   - date = 'most\_recent', 'mm/dd/yy'
#### Example
```json
GET /api/user/get_history_card?username=browna@awty.org&date=12/23/2019
{
   "items": [
      {"id": "1240", "time_out": "16:30", "time_in": "17:45"},
      {"id": "1378", "time_out": "16:33", "time_in": "17:45"},
      {"id": "3558", "time_out": "16:34", "time_in": "17:45"}
   ]
}
```
### `/user/info
#### Params
- username = 'someone@awty.org'
#### Example
```json
GET /api/user/info?username=someone@awty.org
{
   "name": "John Doe",
   "student_id": "100018",
   "last_login": "Sat Jan 4, 2019"
}
```
### `/item/history`
#### Params
- item\_id
   - 4digit number
#### Example
```js
GET /api/item/history?item_id=1234
[
   {
      "user": "someone@awty.org"
      "action": "checkout"
      "date": "Sat Jan 4, 2019"
      "time": "13:34"
   } //, ...
]
```
### `/item/info`
#### Params
- item\_id
#### Example
```json
GET /api/item/info?item_id=1432
{
   "manufacturer": "Canon"
   "type": "lens"
   "make_model": "F2 80mm"
   "is_availible": true
}
```
      
## Objects
### User
- PRIMARY KEY string username (email)
- hash of password+salt
- salt crypto random generated
- history of checkout-checkin dates
sample history:
```js
[
  {
    "checkout": "Mon 23 Dec 2019 04:43:30 PM CST",
    "checkin": "",
    "item": 13002 //global UID
  }, // ...
]
```
### Equipment
- global UID
- Manufacturer
- Make + model
- \# of Unique Item
- is\_availible
### Manufacturer list
- Name of manufacturer
- list of availible makes and models

## SQL to create database
```sql
CREATE TABLE manufacturers(
   manufacturer  TEXT  NOT NULL,
   type          TEXT  NOT NULL,
   model         TEXT  NOT NULL
);

CREATE TABLE users(
   username       TEXT      NOT NULL,
   password_hash  CHAR(256) NOT NULL,
   salt           CHAR(32)  NOT NULL,
   history        TEXT
);

CREATE TABLE equipment(
   uid_global    CHAR(4)  NOT NULL,
   manufacturer  CHAR(25) NOT NULL,
   type          CHAR(25) NOT NULL,
   make_model    CHAR(25) NOT NULL,
   uid_local     INT      NOT NULL,
   is_availible  BOOLEAN
);
```
